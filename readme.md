<div align="right">
    <img src="https://img.shields.io/badge/HTML5-E44D26?style=for-the-badge&logo=html5&logoColor=white" /> <br>
    <img src="https://img.shields.io/badge/CSS3-2965f1?style=for-the-badge&logo=css3&logoColor=white" />
</div>

# Podstawy HTML + CSS w dwie godziny - kurs krok po kroku!


## Opis projektu


Moje portfolio internetowe powstało w ramach kursu na platformie YouTube, gdzie wykorzystałem technologie `HTML i CSS`. Projekt ten jest przedstawieniem interaktywnego **CV** dla przykładowej osoby i stanowił część praktycznych ćwiczeń z użyciem wymienionych wcześniej technologii.

<a href="https://www.youtube.com/watch?v=opNgrPv3Qw8">
<img src="/default/youtube.jpg" width=400>
</a>

## Technologie

Projekt został zrealizowany przy użyciu następujących technologii:

- HTML
- CSS

## Instrukcje uruchomienia

Jeśli chcesz uruchomić projekt lokalnie, sklonuj repozytorium i otwórz plik `index.html` w przeglądarce internetowej.

## Struktura katalogów

```
.
├── default
│   ├── youtube.jpg
├── images
│   ├── basketball.png
│   ├── birthday-cake.png
│   ├── book.png
│   ├── mail.png
│   ├── movie.png
│   ├── music.png
│   ├── phone.png
│   ├── pin.png
│   └── puzzle.png
├── default-avatar.png
├── index.html
├── README.md
└── style.css
```



![Jacek Podgórni](https://img.shields.io/badge/Jacek-Podg%C3%B3rni-teal)<br>
![HTML5](https://img.shields.io/badge/HTML%205-grey?style=for-the-badge&logo=html5)<br>
![CSS3](https://img.shields.io/badge/CSS%203-grey?style=for-the-badge&logo=css3&logoColor=blue)
